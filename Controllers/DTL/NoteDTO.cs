﻿using Notes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAngular.Controllers.DTL
{
    public class NoteDTO
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string[] Tags { get; set; }
        public bool Shared { get; set; }

        public NoteDTO()
        {
            Tags = new string[] { };
        }
        public NoteDTO(Note source)
        {
            List<string> tempTags = new List<string>();
            Id = source.Id;
            UserId = source.UserId;
            TimeStamp = source.TimeStamp;
            Title = source.Title;
            Content = source.Content;

            foreach (Tag tb in source.Tags) {
                tempTags.Add(tb.Text);
            }
            Tags = tempTags.ToArray();
            Shared = source.Shared;
        }
    }
}
