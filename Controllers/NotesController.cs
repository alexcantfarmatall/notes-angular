﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Notes.Model;
using NotesAngular.Controllers.DTL;
using NotesAngular.Services;

namespace Notes.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private INoteService noteService;
        public NotesController(INoteService noteService)
        {
            this.noteService = noteService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            List<Note> notes = noteService.FindAll();

            if (notes == null)
                return NotFound("no notes here");

            List<NoteDTO> noteDTOs = new List<NoteDTO>();

            foreach (Note note in notes)
            {
                noteDTOs.Add(new NoteDTO(note));
            }
        
            return Ok(noteDTOs);
        }

        [HttpGet("tag/{tag}")]
        public IActionResult GetByTag([FromRoute]string tag)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Note> notes = noteService.FindByTag(tag);

            if (notes == null)
            {
                return NotFound();
            }

            List<NoteDTO> noteDTOs = new List<NoteDTO>();

            foreach (Note note in notes)
            {
                noteDTOs.Add(new NoteDTO(note));
            }

            return Ok(noteDTOs);
        }

        [HttpGet("content/{content}")]
        public IActionResult GetByContent([FromRoute]string content)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Note> notes = noteService.FindByContent(content);

            if (notes == null)
            {
                return NotFound();
            }

            List<NoteDTO> noteDTOs = new List<NoteDTO>();

            foreach (Note note in notes)
            {
                noteDTOs.Add(new NoteDTO(note));
            }

            return Ok(noteDTOs);
        }

        [HttpGet("title/{title}")]
        public IActionResult GetByTitle([FromRoute]string title)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Note> notes = noteService.FindByTitle(title);

            if (notes == null)
            {
                return NotFound();
            }

            List<NoteDTO> noteDTOs = new List<NoteDTO>();

            foreach (Note note in notes)
            {
                noteDTOs.Add(new NoteDTO(note));
            }

            return Ok(noteDTOs);
        }

        [HttpGet("{id}")]
        public IActionResult GetById([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Note note = noteService.FindById(id);

            if (note == null)
            {
                return NotFound();
            }

            return Ok(new NoteDTO(note));
        }

        [HttpGet("user/{id}")]
        public IActionResult GetByUserId([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<Note> notes = noteService.FindByUserId(id);

            if (notes == null)
            {
                return NotFound();
            }

            List<NoteDTO> noteDTOs = new List<NoteDTO>();

            foreach (Note note in notes)
            {
                noteDTOs.Add(new NoteDTO(note));
            }
           
            return Ok(noteDTOs);
        }

       

        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromRoute] Guid id, [FromBody] NoteDTO noteDTO)

        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != noteDTO.Id)
            {
                return BadRequest();
            }



            //await noteService.Delete(id);
            //note = await noteService.Create(note);
            // TODO: fix: noteService.Update is not working when tags have been updated
            Note note = await noteService.Update(id, new Note(noteDTO));
            
            return Ok(new NoteDTO(note));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]NoteDTO noteDTO)

        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Note note = await noteService.Create(new Note(noteDTO));

            return CreatedAtAction("POST", new NoteDTO(note));
        }

        // DELETE: api/Notes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var deletedNote = await noteService.Delete(id);

            if (deletedNote != false)
                return Ok(id);
            else
                return NotFound();
        }

        private bool NoteExists(Guid id)
        {
            return noteService.Exists(id);
        }
    }
}