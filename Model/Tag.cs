﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Notes.Model
{
    public class Tag
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("Note")]
        public Guid NoteId { get; set; }
        public Note Note { get; set; }
        public string Text { get; set; }

        public Tag()
        {
            Id = Guid.NewGuid();
        }
        public Tag(string text)
        {
            Id = Guid.NewGuid();
            Text = text;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}