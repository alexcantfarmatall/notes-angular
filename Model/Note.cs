﻿using NotesAngular.Controllers.DTL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Notes.Model
{
    public class Note
    {
        [Key]
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public bool Shared { get; set; }
        [NotMapped]
        public string[] TagsFrontend { get; set; }

        public Note()
        {
            Id = Guid.NewGuid();
            Tags = new List<Tag>();
            TagsFrontend = new string[] { };
            TimeStamp = DateTime.Now;
        }

        public Note(NoteDTO source)
        {
            Id = source.Id;
            UserId = source.UserId;
            TimeStamp = source.TimeStamp;
            Title = source.Title;
            Content = source.Content;
            Tags = new List<Tag>();

            foreach (string st in source.Tags)
            {
                Tag tag = new Tag(st);
                Tags.Add(tag);
            }
            Shared = source.Shared;
            TagsFrontend = source.Tags;
        }

        public void UpdateAllPropertiesFromExistingNote(Note source)
        {
            try
            {
                if (Id == source.Id)
                {
                    UserId = source.UserId;
                    TimeStamp = DateTime.Now;
                    Title = source.Title;
                    Content = source.Content;
                    Tags = source.Tags;
                    Shared = source.Shared;
                    List<string> tmp = new List<string>();
                    foreach (Tag t in source.Tags)
                    {
                        tmp.Add(t.Text);
                    }

                    TagsFrontend = tmp.ToArray();
                } else
                {
                    throw new Exception("cant change note.Id in note.Update");
                }
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
        }
    }
}
