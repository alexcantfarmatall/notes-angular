using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Notes.Model;
using NotesAngular.Controllers.DTL;
using NotesAngular.Services;

namespace NotesAngular
{
    public class Startup
    {
        private string AngularClientLocalhostOrigin = "http://localhost:5002";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AngularClientLocalhostWithAnyHeadersWithAnyMethod",
                    builder => builder.WithOrigins(AngularClientLocalhostOrigin).AllowAnyHeader().AllowAnyMethod());
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<NotesContext>(options => {
                options.UseSqlServer(Configuration.GetConnectionString("NotesContext"));
                options.EnableSensitiveDataLogging(true);
                });
            services.AddLogging();
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            services.AddScoped<INoteService, NoteService>();
            services.AddScoped<IEntityService<Note>, EntityService<Note>>();
            services.AddSingleton<ISearchService, SearchService>();
           
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // app.UseDeveloperExceptionPage();
            }
            else
            {
                // app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            app.UseCors("AngularClientLocalhostWithAnyHeadersWithAnyMethod");
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();           
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });

        }
    }
}
