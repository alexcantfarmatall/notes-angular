import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NoteFormComponent } from './components/note-form/note-form.component';
import { NotesComponent } from './components/notes/notes.component';
import { NoteService } from './services/note.service';
import { HttpClientModule } from '@angular/common/http';
import { NoteSearchComponent } from './components/note-search/note-search.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    NoteFormComponent,
    NotesComponent,
    NoteSearchComponent
  ],
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, HttpClientModule],
  providers: [NoteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
