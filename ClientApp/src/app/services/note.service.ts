import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Note } from '../models/note';
import { SearchInput } from '../models/search-input';
// import { NOTES } from '../models/mock-notes';

@Injectable()
export class NoteService {
  notesUrl = 'https://localhost:44369/api/notes';
  notes: Note[];
  lastSelectedNote = new BehaviorSubject<Note>({
    id: null,
    userId: null,
    timeStamp: null,
    title: null,
    content: null,
    tags: [],
    shared: null
  });
  selectedNote = this.lastSelectedNote.asObservable();
  newOrUpdatedNote = new BehaviorSubject<Note>({
    id: null,
    userId: null,
    timeStamp: null,
    title: null,
    content: null,
    tags: [],

    shared: null
  });
  noteChanges = new BehaviorSubject<boolean>(true);
  // stateClear = this.stateSource.asObservable();
  constructor(private http: HttpClient) { }

  getNotes(input?: SearchInput): Observable<Note[]> {
    console.log('input === undef? ', input);
    if (input) {
      if (input.query.length > 0) {
        this.noteChanges.next(false);
        console.log('GET + search to: ' + this.notesUrl + '/' + input.target + '/' + input.query);
        return this.http.get<Note[]>(this.notesUrl + '/' + input.target + '/' + input.query);
      }
    }

    console.log('GET + no tags');
    this.noteChanges.next(false);
    return this.http.get<Note[]>(this.notesUrl);

  }

  setFormNote(note: Note) {
    this.noteChanges.next(false);
    this.lastSelectedNote.next(note);
  }

  addNote(note: Note): Observable<Note> {
    this.noteChanges.next(true);
    this.newOrUpdatedNote.next(note);
    return this.http.post<Note>(this.notesUrl, note);
  }

  updateNote(note: Note): Observable<Note> {
    // this.noteChanges.next(true);
    // this.newOrUpdatedNote.next(note);
    console.log('PUT ', note);
    return this.http.put<Note>(this.notesUrl + '/' + note.id, note);
  }

  deleteNote(note: Note): Observable<Note> {
    this.noteChanges.next(true);
    return this.http.delete<Note>(this.notesUrl + '/' + note.id);
  }

  clearState() {
    this.noteChanges.next(true);
  }
}
