import { Component, OnInit } from "@angular/core";

import { NoteService } from "../../services/note.service";

import { Note } from "../../models/note";
import { Tag } from "../../models/tag";

@Component({
  selector: "app-notes",
  templateUrl: "./notes.component.html",
  styleUrls: ["./notes.component.css"]
})
export class NotesComponent implements OnInit {
  notes: Note[];
  selectedNote: Note;
  loaded = false;

  constructor(private noteService: NoteService) {}

  ngOnInit() {
    this.notes = [];
    this.noteService.noteChanges.subscribe(changes => {
      if (changes) {
        this.selectedNote = {
          id: "",
          userId: "",
          timeStamp: "",
          title: "",
          content: "",
          tags: [],
          shared: true
        };
      }
    });

    this.noteService.newOrUpdatedNote.subscribe(newOrUpdatedNote => {
      console.log("newOrUpdatedNote: ", newOrUpdatedNote);
      if (newOrUpdatedNote.id !== null) {
        this.notes.forEach((element, index) => {
          if (element.id === newOrUpdatedNote.id) {
            this.notes.splice(index, 1);
          }
        });
        this.notes.push(newOrUpdatedNote);
        console.log(this.notes);
      }
    });

    this.noteService.getNotes().subscribe(notes => {
      this.notes.forEach((element, index) => {
        console.log("got note: ", element);
      });
      this.notes = notes;
      this.loaded = true;
    });
  }

  onSelect(note: Note) {
    console.log("selected note: ", note);
    this.noteService.setFormNote(note);
    this.selectedNote = note;
  }

  onDelete(note: Note) {
    this.notes.forEach((element, index) => {
      if (element.id === note.id) {
        this.notes.splice(index, 1);
      }
    });
    this.noteService.deleteNote(note).subscribe(res => {});
  }
}
