import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { NoteService } from '../../services/note.service';
import { Observable } from 'rxjs';
import { FormBuilder, FormArray } from '@angular/forms';
import { Note } from 'src/app/models/note';

@Component({
  selector: 'app-note-form',
  templateUrl: './note-form.component.html',
  styleUrls: ['./note-form.component.css']
})
export class NoteFormComponent implements OnInit {
  isnew = true;
  noteForm = this.fb.group({
    id: [''],
    userId: [''],
    timeStamp: [Date.now()],
    title: [''],
    content: [''],
    tags: this.fb.array([''])
    // shared: ['']
  });

  constructor(private noteService: NoteService, private fb: FormBuilder) { }

  ngOnInit() {
    // Subscribe to the selectedNote observable
    this.noteService.lastSelectedNote.subscribe(note => {
      if (note.id !== null) {
        this.isnew = false;
      }

      this.noteForm.get('id').setValue(note.id);
      this.noteForm.get('userId').setValue(note.userId);
      this.noteForm.get('timeStamp').setValue(note.timeStamp);
      this.noteForm.get('title').setValue(note.title);
      this.noteForm.get('content').setValue(note.content);

      // have to clear existing tags before pushing new
      // because setValue() didn't work with arrays
      // as it worked for above form fields
      this.clearTags();
      note.tags.forEach(tag => {
        (this.noteForm.get('tags') as FormArray).push(this.fb.control(tag));
      });
      // this.noteForm.get('shared').setValue(note.shared);
    });

    this.clearTags();
  }

  onSubmit() {
    console.log('isNew = ', this.isnew);

    if (this.isnew) {

      // Create new note
      console.log('creating new note');
      const newNote = {
        id: this.generateId(),
        userId: '0f8fad5b-d9cb-469f-a165-708677289589', // DEV hard coded at first
        timeStamp: new Date(),
        title: this.noteForm.get('title').value,
        content: this.noteForm.get('content').value,
        tags: this.noteForm.get('tags').value,
        shared: true // this.noteForm.get('shared').value
      };

      console.log('note going out (new): ', newNote);
      // Add note
      this.noteService.addNote(newNote).subscribe(note => {
        console.log('newnote POST response: ', note);
        this.noteService.newOrUpdatedNote.next(note);
        this.noteService.noteChanges.next(true);
        this.clearState();
      });
    } else {

      // Create note to be updated
      console.log('updating an existing note');
      const updNote = {
        id: this.noteForm.get('id').value,
        userId: this.noteForm.get('userId').value,
        timeStamp: new Date(),
        title: this.noteForm.get('title').value,
        content: this.noteForm.get('content').value,
        tags: this.noteForm.get('tags').value,
        shared: true // this.noteForm.get('shared').value
      };
      console.log('note going out (update): ', updNote);
      // Update note
      this.noteService.updateNote(updNote).subscribe(note => {
        console.log('updated note POST response: ', note);
        this.noteService.newOrUpdatedNote.next(note);
        this.noteService.noteChanges.next(true);
        this.clearState();
      });
    }
  }

  // Helper functions
  get tags() {
    return this.noteForm.get('tags') as FormArray;
  }

  addTag() {
    this.tags.push(this.fb.control(''));
  }
  deleteTag(i: number) {
    // removes tag from tags array
    this.tags.value.splice(i, 1);
    // removes tag input form control
    this.tags.removeAt(i);
  }

  clearTags() {
    const len = this.tags.length;
    if (len > 0) {
      for (let i = len - 1; i >= 0; i--) {
        this.tags.removeAt(i);
      }
    }
  }

  clearState() {
    this.isnew = true;
    this.noteForm.get('id').setValue('');
    this.noteForm.get('userId').setValue('');
    this.noteForm.get('title').setValue('');
    this.noteForm.get('timeStamp').setValue('');
    this.noteForm.get('content').setValue('');
    this.clearTags();
    // this.noteForm.get('shared').setValue('true');
    this.noteService.clearState();
  }

  generateId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      // tslint:disable-next-line:no-bitwise
      const r = (Math.random() * 16) | 0,
        // tslint:disable-next-line:no-bitwise
        v = c === 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }
}
