import { Component, OnInit } from '@angular/core';
import { fromEvent, timer, Observable } from 'rxjs';
//import { debounce, map, where, distinctUntilChanged, } from 'rxjs/operators';
import { NoteService } from 'src/app/services/note.service';
// import { locateHostElement } from '@angular/core/src/render3/instructions';
import { Note } from '../../models/note';
import { SearchInput } from '../../models/search-input';
@Component({
  selector: 'app-note-search',
  templateUrl: './note-search.component.html',
  styleUrls: ['./note-search.component.css']
})
export class NoteSearchComponent implements OnInit {
  notes: Note[] = [];
  constructor(private noteService: NoteService) { }
  input = new SearchInput();
  ngOnInit() {
    this.input.target = 'tag';
  }

  search(event: any) {
    // delay api calls somehow
    this.input.query = event.target.value;
    this.noteService.getNotes(this.input).subscribe(notes => {
      // list notes somewhere
      this.notes = notes;
      console.log('notes by tag: ', notes);
    });
  }
}
