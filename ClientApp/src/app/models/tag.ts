export class Tag {
  // TODO: remove the uses of Tag all and all and use string array only to represent tags.
  // tag id and user id are relevant only at the backend, since searching, sorting, matching and whatnot
  // should be done only at the backend, and finished result are spewd out to client.
  //
  // OR
  // maybe its more efficient to Construct these Tag object on the client side,
  /// and do the searching, sorting and matching locally, since http calls
  // are probably more costly, unless theres loads of notes
  // https://www.quora.com/Should-filtering-be-done-on-the-client-side-by-searching-through-an-array-of-all-data-in-the-database-or-should-it-be-done-on-the-server-level-by-only-returning-data-relevant-to-the-filters
  //
  //MAYBE
  // learning ASPNET is more relevant now, it should be done there...
  id?: string;
  userId?: string;
  text: string;

  public toString = (): string => {
    return this.text;
  };
}
