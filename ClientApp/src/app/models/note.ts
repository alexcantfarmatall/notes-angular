import { Tag } from './tag';
export interface Note {
    id?: string;
    userId: string;
    timeStamp: any;
    title: string;
    content: string;
    // tags: Tag[];
    tags: string[];
    shared: boolean;
}

