export class SearchInput {
    target: string; // tag, content or null
    query: string;
}
