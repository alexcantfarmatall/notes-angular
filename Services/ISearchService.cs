﻿using Notes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAngular.Services
{
    public interface ISearchService
    {
        List<Note> Search(string needle, Note haystack);
    }
}
