﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Notes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NotesAngular.Services
{
    public class EntityService<TEntity> : IEntityService<TEntity> where TEntity : class
    {
        internal NotesContext context;
        internal DbSet<TEntity> dbSet;

        public EntityService(NotesContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = dbSet;

            if(filter != null)
            {
                query = query.Where(filter);
            }

            foreach(var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if(orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public virtual TEntity GetByID(object id)
        {
            return dbSet.Find(id);
        }

        public virtual async Task<TEntity> Insert(TEntity entity)
        {
            var result = dbSet.Add(entity).Entity;
            await context.SaveChangesAsync();
            return result;
        }

        public virtual async Task<TEntity> Delete(object id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            return await Delete(entityToDelete);
        }

        public virtual async Task<TEntity> Delete(TEntity entityToDelete)
        {
            if(context.Entry(entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach(entityToDelete);
            }

            dbSet.Remove(entityToDelete);

            try
            {
                await context.SaveChangesAsync();

            }catch(Exception e)
            {
                
            }
            return entityToDelete;
        }

        public virtual async Task<TEntity> Update(TEntity entityToUpdate)
        {
            TEntity result = dbSet.Attach(entityToUpdate).Entity;
            context.Entry(entityToUpdate).State = EntityState.Modified;

            await context.SaveChangesAsync();
            return result;
        }
    }
}
