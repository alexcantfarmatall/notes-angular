﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Notes.Model;

namespace NotesAngular.Services
{
    public class NoteService : INoteService
    {
        private readonly ILogger<NoteService> logger;
        private readonly NotesContext context;

        public NoteService(NotesContext context, ILogger<NoteService> logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public List<Note> FindAll()
        {
            return context.Notes.Include(n => n.Tags).AsNoTracking().ToList();
        }

        public List<Note> FindByTag(string text)
        {
            IQueryable<Note> notes = context.Notes.AsNoTracking().SelectMany(n => n.Tags).Where(t => t.Text == text).Select(t => t.Note);
            return notes.ToList();
        }

        public List<Note> FindByContent(string snippet)
        {
            IQueryable<Note> notes = context.Notes.AsNoTracking().Where(c => c.Content.Contains(snippet));
            return notes.ToList();
        }

        public List<Note> FindByTitle(string title)
        {
            IQueryable<Note> notes = context.Notes.AsNoTracking().Where(c => c.Title.Contains(title));
            return notes.ToList();
        }

        public async Task<Note> Create(Note note)
        {
            var result = context.Notes.Add(note);
            
            await context.SaveChangesAsync();

            return result.Entity;
        }

        public async Task<bool> Delete(Guid id)
        {
            // Q: AsNoTracking()?
            var note = context.Notes.AsNoTracking().Include(n => n.Tags).FirstOrDefault(n => n.Id == id);

            if (note == null)
                return false;

            context.Notes.Remove(note);

            await context.SaveChangesAsync();

            return true;
        }

        public Note FindById(Guid id)
        {
            return context.Notes.Include(n => n.Tags).First(note => note.Id == id);
        }

    

        public List<Note> FindByUserId(Guid id)
        {
            return context.Notes.Include(n => n.Tags).AsNoTracking().Where(note => note.UserId == id).ToList();
        }

        public bool Exists(Guid id)
        {
            var note = context.Notes.AsNoTracking().FirstOrDefault(n => n.Id == id);

            return note == null ? false : true;
        }
        public async Task<Note> Update(Guid id, Note updNote)
        {
            Note note = context.Notes.Include(n => n.Tags).FirstOrDefault(n => n.Id == id);

            if (updNote == null)
            {
                return null;
            }
            note.UpdateAllPropertiesFromExistingNote(updNote);

            // context.Attach(updNote);//.State = EntityState.Modified;
            //foreach (Tag t in updNote.Tags)
            //{
            //    context.Update(t).State = EntityState.Modified;
            //}

            try
            {
                await context.SaveChangesAsync();

                return updNote;
            }
            catch (DbUpdateConcurrencyException)
            {
                Console.WriteLine("not found");
                return null;
            }   
        }   
    }
}
