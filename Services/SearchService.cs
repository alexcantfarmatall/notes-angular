﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Notes.Model;

namespace NotesAngular.Services
{
    public class SearchService : ISearchService
    {
        public List<Note> Search(string needle, Note haystack)
        {
            List<string> normalizedContent = Normalize(haystack.Content);
            List<string> normalizedNeedle = Normalize(needle);
            Dictionary<string, int> contentWordsAndCounts = CountWords(normalizedContent);
            Dictionary<string, int> needleWordsAndCounts = CountWords(normalizedNeedle);
            // corpus is a set of all the words at play
            SortedSet<string> corpus = Corpusize(normalizedNeedle.Concat(normalizedContent).ToList());
            
            // make frequency vectors

            return null;
        }

        private Dictionary<string, int> CountWords(List<string> text)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            foreach(string s in text)
            {
               
                if(result.TryGetValue(s,out int i))
                {
                    result[s] = i;
                } else
                {
                    result.Add(s, 1);
                }
            }

            return result;
        }

        private SortedSet<string> Corpusize(List<string> l)
        {
            SortedSet<string> result = new SortedSet<string>();
            foreach(string s in l)
            {
                result.Add(s);
            }
            return result;
        }

        /*
         * Removes all but alphanumeric characters
         */
        private List<String> Normalize(string content)
        {
            StringBuilder sb = new StringBuilder();

            foreach (char c in content)
            {

                if (c.ToString().All(char.IsLetterOrDigit))
                {
                    sb.Append(c);
                }
            }

            List<string> result = sb.ToString().Split(' ').ToList();

            return result;
        }
    }
}
