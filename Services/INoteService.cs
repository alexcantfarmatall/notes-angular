﻿using Notes.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAngular.Services
{
    public interface INoteService
    {
        List<Note> FindAll();
        Note FindById(Guid id);
        List<Note> FindByUserId(Guid id);
        List<Note> FindByTitle(string title);
        Task<Note> Update(Guid id,Note note);
        Task<Note> Create(Note note);
        Task<bool> Delete(Guid id);
        bool Exists(Guid id);
        List<Note> FindByTag(string text);
        List<Note> FindByContent(string snippet);


    }
}
